use super::*;

#[test]
fn returns_rows() {
    let m = Matrix::new(1, 4, vec![1, 2, 3, 4]);

    assert_eq!(m.rows(), 1);
}

#[test]
fn returns_cols() {
    let m = Matrix::new(1, 1, vec![0]);

    assert_eq!(m.cols(), 1);
}

#[test]
fn gets_correct_element() {
    let m = Matrix::new(2, 2, vec![1, 4, 12, -3]);

    assert_eq!(m.at(1, 0), 12);
}

#[test]
#[should_panic]
fn panics_when_out_of_bounds() {
    let m = Matrix::new(4, 1, vec![1, 4, 12, -3]);

    m.at(1, 2);
}

#[test]
fn mutates_element_at() {
    let mut m = Matrix::new(1, 1, vec![5]);

    *m.at_mut(0, 0) = 4;

    assert_eq!(m.at(0, 0), 4);
}

#[test]
fn maps_properly() {
    let m = Matrix::new(2, 2, vec![5; 4]);

    let mapped = m.map(|&el| el + 8);

    assert_eq!(*mapped.data(), vec![13; 4]);
}

#[test]
fn adds_two_matrices() {
    let m1 = Matrix::new(3, 3, vec![1, 2, 3, 4, 4, 5, 6, 7, 8]);
    let m2 = Matrix::new(3, 3, vec![1, 29, 3, 5, 3, -6, 3, 7, 2]);

    let m = &m1 + &m2;

    assert_eq!(*m.data(), vec![2, 31, 6, 9, 7, -1, 9, 14, 10]);
    assert_eq!(m.rows(), 3);
    assert_eq!(m.cols(), 3);
}

#[test]
#[should_panic]
fn add_panics_when_matrices_differ_in_size() {
    // utilize an arbitrary large vector
    let count: usize = 50000000;
    let v = vec![5; count];

    let m1 = Matrix::from_vec(1, count, &v);
    let m2 = Matrix::new(count, 1, v);

    let _m = &m1 + &m2;
}

#[test]
fn matrix_multiplication_is_not_commutative() {
    let m1 = Matrix::new(2, 3, vec![3, 2, 1, 4, 8, 5]);
    let m2 = Matrix::new(3, 2, vec![5, 8, -1, 12, 0, 6]);

    let m = &m1 * &m2;
    let n = &m2 * &m1;

    assert_eq!(m.rows(), 2);
    assert_eq!(m.cols(), 2);
    assert_eq!(*m.data(), vec![13, 54, 12, 158]);

    assert_eq!(n.rows(), 3);
    assert_eq!(n.cols(), 3);
    assert_eq!(*n.data(), vec![47, 74, 45, 45, 94, 59, 24, 48, 30]);
}

#[test]
#[should_panic]
fn mult_panics_when_dimensions_dont_match() {
    let m1 = Matrix::new(1, 3, vec![3, 2, 1]);
    let m2 = Matrix::new(4, 2, vec![3, 2, 1, 3, 5, 1, 6, 7]);

    let _m = &m1 * &m2;
}

#[test]
fn returns_diagonal_when_matrix_is_square() {
    let m = Matrix::new(3, 3, vec![4, 1, 3, 51, 5, -23, 99, 50, 6]);

    assert_eq!(m.diagonal(), vec![4, 5, 6]);
}

#[test]
#[should_panic]
fn diagonal_panics_when_matrix_is_not_square() {
    let m = Matrix::new(2, 3, vec![4, 1, 3, 51, 5, -23]);

    m.diagonal();
}

// TODO: implement determinant
//#[test]
//#[should_panic]
//fn determinant_panics_when_matrix_is_not_square() {
//    let m = Matrix::new(2, 3, vec![4, 1, 3, 51, 5, -23]);

//    m.determinant();
//}

//#[test]
//fn returns_correct_determinant() {
//    let m = Matrix::new(3, 3, vec![1; 9]);

//    assert_eq!(m.determinant(), 0);
//}
